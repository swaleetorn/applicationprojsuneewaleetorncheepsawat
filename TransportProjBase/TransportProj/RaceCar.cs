﻿using System;

namespace TransportProj
{
    public class RaceCar : Car
    {
        private int spaces = 2;

        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos += spaces;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos -= spaces;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos += spaces;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos -= spaces;
                WritePositionToConsole();
            }
        }

        public override void Move(int destXPos, int destYPos)
        {
            if (destXPos > City.XMax)
                destXPos = City.XMax;

            if (destXPos < 0)
                destXPos = 0;

            if (destYPos > City.YMax)
                destYPos = City.YMax;

            if (destYPos < 0)
                destYPos = 0;

            spaces = 2; //move 2 spaces first
            if (XPos < destXPos-1)
                MoveRight();
            else if (XPos > destXPos+1)
                MoveLeft();
            else if (YPos < destYPos-1)
                MoveUp();
            else if (YPos > destYPos+1)
                MoveDown();
            else
            {
                spaces = 1;
                if (XPos < destXPos)
                    MoveRight();
                else if (XPos > destXPos)
                    MoveLeft();
                else if (YPos < destYPos)
                    MoveUp();
                else if (YPos > destYPos)
                    MoveDown();
            }
        }

    }
}
