﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportProj.Factory;

namespace TransportProj
{
    public class BindModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<ICarFactory>().To<CarFactory>().InSingletonScope();
            Bind<City>().ToSelf();
        }
    }
}
