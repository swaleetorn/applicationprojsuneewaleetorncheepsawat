﻿using Ninject;
using Ninject.Parameters;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportProj.Factory;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncContext.Run(() => MainAsync(args));
        }

        private static IKernel NinjectBootStrapper()
        {
            IKernel kernel = new StandardKernel(new BindModule());
            var carFactory = kernel.Get<ICarFactory>();
            carFactory.registerCar(CarType.RaceCar, typeof(RaceCar));
            carFactory.registerCar(CarType.Sedan, typeof(Sedan));

            return kernel;
        }

        static async void MainAsync(string[] args)
        {
            var kernel = NinjectBootStrapper();

            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            var MyCity = kernel.Get<City>(new ConstructorArgument("xMax", CityLength),
                                            new ConstructorArgument("yMax", CityWidth));

            Car car = MyCity.AddCarToCity(CarType.RaceCar, rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            var tasks = new List<Task<string>>();
            while (!passenger.IsAtDestination())
            {
                Tick(car, passenger);
            }

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            if (car.isCarEmpty() && car.isCarHere(passenger.StartingXPos, passenger.StartingYPos))
                passenger.GetInCar(car);
            else
            {
                car.Move(passenger.GetCarDestinationXPos(), passenger.GetCarDestinationYPos());

                if (passenger.isInCar())
                {
                    loadPage();
                }
            }
        }

        private static async void loadPage()
        {
            var content = await LoadSite.Load();
            Console.WriteLine(content);

        }
    }

}


