﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Factory
{
    public interface ICarFactory
    {
        bool registerCar(CarType carType, Type cls);
        Car CreateCar(CarType carType, int xPos, int yPos, City city, Passenger passenger);
    }
}
