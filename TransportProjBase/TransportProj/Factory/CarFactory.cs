﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Factory
{
    public enum CarType
    {
        Sedan,
        RaceCar
    }

    public class CarFactory: ICarFactory
    {
        private Dictionary<CarType, Type> CarMap;
        public CarFactory()
        {
            CarMap = new Dictionary<CarType, Type>();
        }

        public bool registerCar(CarType carType, Type cls)
        {
            if (cls.IsSubclassOf(typeof(Car)))
            {
                CarMap[carType] = cls;
                return true;
            }
            return false;
        }
        
        public Car CreateCar(CarType carType, int xPos, int yPos, City city, Passenger passenger)
        {
            Type type = null;
            CarMap.TryGetValue(carType, out type);

            if (type != null)
            {
                object[] parameters = new object[] { xPos, yPos, city, passenger };
                if(type.IsSubclassOf(typeof(Car)))
                    return (Car)Activator.CreateInstance(type, parameters);
            }

            return null;

        }
    }
}
