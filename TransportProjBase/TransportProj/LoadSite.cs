﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public static class LoadSite
    {
        public static async Task<string> Load()
        {
            HttpClient client = new HttpClient();
            return await client.GetStringAsync("http://www.veyo.com/");
        }

    }
}
