﻿
using Ninject;
using System;
using TransportProj.Factory;

namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        [Inject]
        public ICarFactory CarFactory { private get; set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddCarToCity(CarType carType, int xPos, int yPos)
        {
            
            return CarFactory.CreateCar(carType, xPos, yPos, this, null);
        }


        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

    }
}
