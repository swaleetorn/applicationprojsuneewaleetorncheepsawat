﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;

            Console.WriteLine("Car start at {0},{1}.", XPos, YPos);

        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(string.Format("{0} moved to x - {0} y - {1}", this.GetType().Name, XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();

        public virtual void Move(int destXPos, int destYPos)
        {
            if (XPos < destXPos)
                MoveRight();
            else if (XPos > destXPos)
                MoveLeft();
            else if (YPos < destYPos)
                MoveUp();
            else if (YPos > destYPos)
                MoveDown();
        }

        public bool isCarEmpty()
        {
            return Passenger == null;
        }

        public bool isCarHere(int xPos, int yPos)
        {
            return xPos == XPos && yPos == YPos;
        }
    }
}
